import React from 'react';
import ReactDOM from 'react-dom';
import {Switch, Route} from 'react-router-dom'
import {Books} from './books-table/books';
import {BookInfo} from './books-table/book-info';
import {About} from './about';

export function Main(){
    return (
        <div id="main" className="row margin-top-50">
            <div className="col">
                <h1>Google Books API Test</h1>
                <h3>Built with React by Stephan Victor Schønbeck</h3>
                <Switch>
                    <Route exact path='/' component={Books} />
                    <Route path='/book/:id' component={BookInfo} />
                    <Route path='/about' component={About} />
                </Switch>
            </div>
        </div>
    );
}
