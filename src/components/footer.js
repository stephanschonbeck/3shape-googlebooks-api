import React from 'react';

export function Footer(){
    return(
        <footer className="row margin-top-50">
            <div className="col text-center">
                <p>3Shape Job Test - Created by Stephan Victor Schønbeck</p> 
            </div>
        </footer>
    );
}
