import React from 'react';
import {Link} from 'react-router-dom';

export function Header(){
    return(
        <header className="row">
            <div className="col">
                <nav>
                    <ul>
                        <li><Link to='/'>Home</Link></li>
                        <li><Link to='/about'>About</Link></li>
                    </ul>
                </nav>
            </div>
        </header>
    );
} 
