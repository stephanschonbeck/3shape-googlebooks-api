import React from 'react';
import ReactDOM from 'react-dom';
import {Header} from './header';
import {Main} from './main';
import {Footer} from './footer'; 

export function App(){
    return(
        <div id="wrap">
            <Header />
            <Main />
            <Footer />
        </div>
    );
}
