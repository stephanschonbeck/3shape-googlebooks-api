import React from 'react';
import {BookRow} from './book-row';

export function BooksTable(props){
    return (
        <table className="table margin-top-50">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Publisher</th>
                    <th>Price</th>
                    <th>Link</th>
                </tr>
            </thead>
                <BookRow rows={props.rows}/>
        </table>
    );
}
