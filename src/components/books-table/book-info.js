import React from 'react';
import Iframe from 'react-iframe';

export function BookInfo(props){
    var bookUrl =  window.atob(props.match.params.id);
    console.log(bookUrl);
    return(
        <Iframe url={bookUrl + "&hl=da&pg=PT167&output=embed"}
                width="100%"
                height="1000px"
                display="initial"
                position="relative"
                allowFullScreen/>
    );
}
