import React from 'react';

export function TotalPrice(props){
    return <div className="col"><h4>Total price: {props.amount} kr.</h4></div>;
}
