import React from 'react';

export function BookRow(props){
    return(
        <tbody>
            {props.rows}
        </tbody>
    );
}
