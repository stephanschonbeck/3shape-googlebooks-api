import React from 'react';
import ReactDOM from 'react-dom';
import {Link} from 'react-router-dom';
import {BooksTable} from './table';
import {TotalPrice} from './total-price';
import {TotalBooks} from './total-books';

export class Books extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            apiData: '',
            totalPrice:'', 
            totalBooks: ''
        };
    }
    
    componentDidMount(){
        var randomBookAmount = Math.floor((Math.random()*10)+10);

        // AJAX Call to Google Books API
        var apiRequest = "https://www.googleapis.com/books/v1/volumes?q=3shape&maxResults=" + randomBookAmount  + "&filter=paid-ebooks&fields=items(volumeInfo(title,publisher,previewLink),saleInfo(retailPrice/amount))";

        fetch(apiRequest).then(function(response){
            return response.text();
        }).then(
            (data) => {
                var parsedData = JSON.parse(data);
                var dataArray = parsedData.items;
                var price = 0;
                var rowData = [];

                for (var i = 0; i < dataArray.length; i++) {
                    var volumeData = dataArray[i].volumeInfo;
                    var saleData = dataArray[i].saleInfo;
                    var base64url = window.btoa(volumeData.previewLink);

                    price += saleData.retailPrice.amount;

                    rowData.push(
                          <tr>
                            <td>{volumeData.title}</td>
                            <td>{volumeData.publisher}</td>
                            <td>{saleData.retailPrice.amount + " kr."}</td>
                          <td><Link to={"/book/" + base64url}>Information</Link></td>
                        </tr>
                    );
                }

                this.setState({apiData: rowData, totalPrice: price.toFixed(2), totalBooks: randomBookAmount});
            }
        );
    }

    render(){
        return(
            <div>
                <BooksTable rows={this.state.apiData} />
                <div className="margin-top-50">
                    <div className="row">
                        <TotalPrice amount={this.state.totalPrice} />
                        <TotalBooks totalBooks={this.state.totalBooks} />
                    </div>
                </div>
            </div>
        );
    }
}
