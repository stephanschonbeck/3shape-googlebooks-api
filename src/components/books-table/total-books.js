import React from 'react';

export function TotalBooks(props){
    return <div className="col"><h4>Total books: {props.totalBooks}</h4></div>;
}
